# TEMPLATE IMAGE

This is a template for how images should be structured. In this README, you should give some details about the image, including:

- kernel in use
- variant in use
- arch it targets
- whether or not the image is a live os & ships w/ installer
- output files generated for this image (i.e. carbonOS-VERSION.iso or carbonOS-VERSION-pinephone.raw)
- how to install this image onto the target device
