kind: autotools

# TODO: Use git-tag if there's a release after Jun 20, 2021
# https://github.com/canonical/lightdm/releases
# manual-updates
sources:
- kind: git
  url: github:canonical/lightdm
  track: main
- kind: local
  path: files/lightdm
  directory: carbon

depends:
- pkgs/glib.bst
- pkgs/xorg/libxdmcp.bst
- pkgs/xorg/libxcb.bst
- pkgs/xorg/libxklavier.bst
- pkgs/xorg/libx11.bst
- pkgs/polkit.bst
- pkgs/accounts-service.bst
- pkgs/libgcrypt.bst

build-depends:
- pkgs/intltool.bst
- pkgs/yelp-tools.bst
- pkgs/vala/all.bst
- buildsystems/autotools.bst

variables:
  autogen: autoreconf -ivf
  conf-local: >-
    --disable-tests

config:
  configure-commands:
    (<):
    - touch gtk-doc.make
  install-commands:
    (>):
    - rm -r %{install-root}%{sysconfdir}/{init,apparmor.d}
    - mv %{install-root}%{sysconfdir}/pam.d %{install-root}%{libdir}/pam.d
    - rm %{install-root}%{libdir}/pam.d/lightdm-autologin
    - install -Dvm644 carbon/lightdm.service -t %{install-root}%{libdir}/systemd/system
    - install -Dvm755 carbon/lightdm-session-wrapper -t %{install-root}%{libexecdir}
    - install -Dvm644 carbon/lightdm.tmpfiles %{install-root}%{libdir}/tmpfiles.d/lightdm.conf
    - install -Dvm644 carbon/lightdm.sysusers %{install-root}%{libdir}/sysusers.d/lightdm.conf
    - install -Dvm644 carbon/lightdm.pam %{install-root}%{libdir}/pam.d/lightdm
    - install -Dvm644 carbon/lightdm.conf %{install-root}%{datadir}/lightdm/lightdm.conf.d/carbon.conf
